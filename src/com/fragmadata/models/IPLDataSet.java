package com.fragmadata.models;

import java.util.List;

public class IPLDataSet {

	private List<Deliveries> deliveriesList;
	private List<Matches> matchesList;
	public List<Deliveries> getDeliveriesList() {
		return deliveriesList;
	}
	public void setDeliveriesList(List<Deliveries> deliveriesList) {
		this.deliveriesList = deliveriesList;
	}
	public List<Matches> getMatchesList() {
		return matchesList;
	}
	public void setMatchesList(List<Matches> matchesList) {
		this.matchesList = matchesList;
	}
	
	
}
