package com.fragmadata.models;

public class NetRateModel {

	private String team;
	private float netRate;
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public float getNetRate() {
		return netRate;
	}
	public void setNetRate(float netRate) {
		this.netRate = netRate;
	}
	
	
}
