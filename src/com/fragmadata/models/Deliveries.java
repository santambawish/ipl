package com.fragmadata.models;

public class Deliveries {

	private String matchId;
	private String inning;
	private String battingTeam;
	private String bowlingTeam;
	private String over;
	private String ball;
	private String batsman;
	private String bowler;
	private String wideRuns;
	private String byeRuns;
	private String legByeRuns;
	private String noBallRuns;
	private String penaltyRuns;
	private String batsmanRuns;
	private String extraRuns;
	private String totalRuns;
	public String getMatchId() {
		return matchId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}
	public String getInning() {
		return inning;
	}
	public void setInning(String inning) {
		this.inning = inning;
	}
	public String getBattingTeam() {
		return battingTeam;
	}
	public void setBattingTeam(String battingTeam) {
		this.battingTeam = battingTeam;
	}
	public String getBowlingTeam() {
		return bowlingTeam;
	}
	public void setBowlingTeam(String bowlingTeam) {
		this.bowlingTeam = bowlingTeam;
	}
	public String getOver() {
		return over;
	}
	public void setOver(String over) {
		this.over = over;
	}
	public String getBall() {
		return ball;
	}
	public void setBall(String ball) {
		this.ball = ball;
	}
	public String getBatsman() {
		return batsman;
	}
	public void setBatsman(String batsman) {
		this.batsman = batsman;
	}
	public String getBowler() {
		return bowler;
	}
	public void setBowler(String bowler) {
		this.bowler = bowler;
	}
	public String getWideRuns() {
		return wideRuns;
	}
	public void setWideRuns(String wideRuns) {
		this.wideRuns = wideRuns;
	}
	public String getByeRuns() {
		return byeRuns;
	}
	public void setByeRuns(String byeRuns) {
		this.byeRuns = byeRuns;
	}
	public String getLegByeRuns() {
		return legByeRuns;
	}
	public void setLegByeRuns(String legByeRuns) {
		this.legByeRuns = legByeRuns;
	}
	public String getNoBallRuns() {
		return noBallRuns;
	}
	public void setNoBallRuns(String noBallRuns) {
		this.noBallRuns = noBallRuns;
	}
	public String getPenaltyRuns() {
		return penaltyRuns;
	}
	public void setPenaltyRuns(String penaltyRuns) {
		this.penaltyRuns = penaltyRuns;
	}
	public String getBatsmanRuns() {
		return batsmanRuns;
	}
	public void setBatsmanRuns(String batsmanRuns) {
		this.batsmanRuns = batsmanRuns;
	}
	public String getExtraRuns() {
		return extraRuns;
	}
	public void setExtraRuns(String extraRuns) {
		this.extraRuns = extraRuns;
	}
	public String getTotalRuns() {
		return totalRuns;
	}
	public void setTotalRuns(String totalRuns) {
		this.totalRuns = totalRuns;
	}
	
	
}
