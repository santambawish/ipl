package com.fragmadata.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fragmadata.models.Matches;
import com.fragmadata.models.Result1;

public class Result1Service {

	public void findResult(List<Matches> matchesList){

		List<Result1> result1_2016 = new ArrayList<>();
		List<Result1> result1_2017 = new ArrayList<>();

		Map<String,Integer> teamMap2016 = new HashMap<>();
		Set<String> teamSet2016 = new HashSet<>();

		Map<String,Integer> teamMap2017 = new HashMap<>();
		Set<String> teamSet2017 = new HashSet<>();

		//find toss winners who elected field count in 2016 and 2017
		for(Matches match: matchesList) {

			if("2016".equals(match.getSeason()) && "field".equalsIgnoreCase(match.getTossDecision())) {
				if(teamSet2016.add(match.getTossWinner())) {
					teamMap2016.put(match.getTossWinner(), 1);
				}
				else {
					int tossWins = teamMap2016.get(match.getTossWinner()) +1;
					teamMap2016.put(match.getTossWinner(), tossWins);
				}
			}

			if("2017".equals(match.getSeason()) && "field".equalsIgnoreCase(match.getTossDecision())) {
				if(teamSet2017.add(match.getTossWinner())) {
					teamMap2017.put(match.getTossWinner(), 1);
				}
				else {
					int tossWins = teamMap2017.get(match.getTossWinner()) +1;
					teamMap2017.put(match.getTossWinner(), tossWins);
				}
			}
		}

		for(String teamName : teamSet2016) {
			Result1 result1 = new Result1();
			result1.setYear("2016");
			result1.setTeam(teamName);
			result1.setCount(teamMap2016.get(teamName));
			result1_2016.add(result1);
		}
		for(String teamName : teamSet2017) {
			Result1 result1 = new Result1();
			result1.setYear("2017");
			result1.setTeam(teamName);
			result1.setCount(teamMap2017.get(teamName));
			result1_2017.add(result1);
		}

		//Sort and print Result
		System.out.println("---------------------------------------Result1-----------------------------------------------");	
		System.out.println("Year" +" | "+ "TEAM"+ " | "+"COUNT");
		this.SortOnCountAndPrintTop4(result1_2016);
		this.SortOnCountAndPrintTop4(result1_2017);		

	}
	public void SortOnCountAndPrintTop4(List<Result1> resultList){

		Collections.sort(resultList, new Comparator<Result1>() {
			@Override public int compare(Result1 a, Result1 b) {
				return b.getCount() - a.getCount(); // Descending
			}
		});

		int i = 0 ;	
		for(Result1 result: resultList ) {
			if(i<4) {
				System.out.println(result.getYear()+" | "+ result.getTeam() +" | "+result.getCount());
			}
			i++;
		}
	}
}
