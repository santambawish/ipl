package com.fragmadata.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fragmadata.models.Deliveries;
import com.fragmadata.models.IPLDataSet;
import com.fragmadata.models.Matches;

public class Result2Service {

	public void findResult2(IPLDataSet iplData	) {
		
		Set<String> teamSet2016 = new HashSet<>();
		Set<String> teamSet2017 = new HashSet<>();
		
		
		Map<String,Integer> teamMap2016Fours = new HashMap<>();
		Map<String,Integer> teamMap2016Sixes = new HashMap<>();
		Map<String,Integer> teamMap2016TotalScore = new HashMap<>();
		
		Map<String,Integer> teamMap2017Fours = new HashMap<>();
		Map<String,Integer> teamMap2017Sixes = new HashMap<>();
		Map<String,Integer> teamMap2017TotalScore = new HashMap<>();
		
		for (Matches match: iplData.getMatchesList()) {
			
			if("2016".equals(match.getSeason()) ) {
				
				for(Deliveries deliveries : iplData.getDeliveriesList()) {
				if( match.getMatchId().equalsIgnoreCase(deliveries.getMatchId()) ) {
					teamSet2016.add(deliveries.getBattingTeam());	
					if("4".equalsIgnoreCase(deliveries.getBatsmanRuns()) ){
							if(null == teamMap2016Fours.get(deliveries.getBattingTeam())) {
								teamMap2016Fours.put(deliveries.getBattingTeam(), 1);
							}
							else {
								int Count = teamMap2016Fours.get(deliveries.getBattingTeam())+1;
								teamMap2016Fours.put(deliveries.getBattingTeam(), Count);	
							}
						}
						
						if("6".equalsIgnoreCase(deliveries.getBatsmanRuns()) ){
							if(null == teamMap2016Sixes.get(deliveries.getBattingTeam())) {
								teamMap2016Sixes.put(deliveries.getBattingTeam(), 1);
							}
							else {
								int Count = teamMap2016Sixes.get(deliveries.getBattingTeam())+1;
								teamMap2016Sixes.put(deliveries.getBattingTeam(), Count);	
							}
						}
						
						if(null == teamMap2016TotalScore.get(deliveries.getBattingTeam())) {
							teamMap2016TotalScore.put(deliveries.getBattingTeam(), new Integer(deliveries.getTotalRuns()));
						}
						
						else {
							int count = teamMap2016TotalScore.get(deliveries.getBattingTeam())+new Integer(deliveries.getTotalRuns());
							teamMap2016TotalScore.put(deliveries.getBattingTeam(), count);
						}
							
					}
				}
			}
			

			
			if("2017".equals(match.getSeason()) ) {
				
				for(Deliveries deliveries : iplData.getDeliveriesList()) {
				if( match.getMatchId().equalsIgnoreCase(deliveries.getMatchId())) {
					 
					teamSet2017.add(deliveries.getBattingTeam());	
					
					if("4".equalsIgnoreCase(deliveries.getBatsmanRuns()) ){
							if(null == teamMap2017Fours.get(deliveries.getBattingTeam())) {
								teamMap2017Fours.put(deliveries.getBattingTeam(), 1);
							}
							else {
								int Count = teamMap2017Fours.get(deliveries.getBattingTeam())+1;
								teamMap2017Fours.put(deliveries.getBattingTeam(), Count);	
							}
						}
						
						if("6".equalsIgnoreCase(deliveries.getBatsmanRuns()) ){
							if(null == teamMap2017Sixes.get(deliveries.getBattingTeam())) {
								teamMap2017Sixes.put(deliveries.getBattingTeam(), 1);
							}
							else {
								int Count = teamMap2017Sixes.get(deliveries.getBattingTeam())+1;
								teamMap2017Sixes.put(deliveries.getBattingTeam(), Count);	
							}
						}
						
						if(null == teamMap2017TotalScore.get(deliveries.getBattingTeam())) {
							teamMap2017TotalScore.put(deliveries.getBattingTeam(), new Integer(deliveries.getTotalRuns()));
						}
						
						else {
							int count = teamMap2017TotalScore.get(deliveries.getBattingTeam())+new Integer(deliveries.getTotalRuns());
							teamMap2017TotalScore.put(deliveries.getBattingTeam(), count);
						}
							
					}
				}
			}
		
		}
		
		System.out.println("----------------------Result2-----------------------");
		System.out.println("YEAR"+ " | "+ "TEAM"+" | "+"FOURS_COUNT"+" | "+"SIXES_COUNT"+" | "+"TOTAL_RUNS");
		
		for(String team : teamSet2016) {
			System.out.println("2016"+ " | "+team+" | "+teamMap2016Fours.get(team)+" | "+teamMap2016Sixes.get(team)+" | "+teamMap2016TotalScore.get(team));
		}
	}
}
