package com.fragmadata.service;

import com.fragmadata.models.IPLDataSet;

public class ComputationService {

	public void process(String matches, String deliveries) {
		
		DataService data = new DataService();
		Result1Service result1Service = new Result1Service();
		Result2Service result2Service = new Result2Service();
		Result3Service result3Service = new Result3Service();
		Result4Service result4Service = new Result4Service();
		
		IPLDataSet iplData= data.getIplData(matches,deliveries);
		
		
		result1Service.findResult(iplData.getMatchesList());
		result2Service.findResult2(iplData);
		result3Service.findResult3(iplData);
		result4Service.findResult4(iplData);
	}
	
}
