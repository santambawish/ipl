package com.fragmadata.service;

import java.util.List;

import com.fragmadata.models.Deliveries;
import com.fragmadata.models.IPLDataSet;
import com.fragmadata.models.Matches;
import com.fragmadata.readfile.ReadFile;

public class DataService {

	ReadFile readFile = new ReadFile();

	public IPLDataSet getIplData(String matches, String deliveries) {

		IPLDataSet iplData = new IPLDataSet();

		iplData.setDeliveriesList(this.getDeliveries(deliveries));
		iplData.setMatchesList(this.getMatches(matches));

		return iplData;

	}

	public List<Deliveries> getDeliveries(String deliveries) {

		return (readFile.readDeliveries(deliveries));
	}

	public List<Matches> getMatches(String matches) {

		return (readFile.readMatches(matches));
	}
}
