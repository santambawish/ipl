package com.fragmadata.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fragmadata.models.Deliveries;
import com.fragmadata.models.IPLDataSet;
import com.fragmadata.models.Matches;

public class Result3Service {

	public void findResult3(IPLDataSet iplData) {

		Set<String> bowlersSet2016 = new HashSet<>();
		Set<String> bowlersSet2017 = new HashSet<>();
		
		Map<String,Integer> bowlerMap2016RunsGiven = new HashMap<>();
		Map<String,Integer> bowlerMap2017RunsGiven = new HashMap<>();
		
		Map<String,Integer> bowlerMap2016OversBowled = new HashMap<>();
		Map<String,Integer> bowlerMap2017OversBowled = new HashMap<>();

		for (Matches match: iplData.getMatchesList()) {

			if("2016".equals(match.getSeason()) ) {

				for(Deliveries deliveries : iplData.getDeliveriesList()) {
					if( match.getMatchId().equalsIgnoreCase(deliveries.getMatchId()) ) {
						bowlersSet2016.add(deliveries.getBowler());
						int runs = new Integer(deliveries.getTotalRuns() ) -new Integer(deliveries.getByeRuns()) - new Integer(deliveries.getLegByeRuns());
						
						if(null == bowlerMap2016RunsGiven.get(deliveries.getBowler())) {
							bowlerMap2016RunsGiven.put(deliveries.getBowler(), runs);
							bowlerMap2016OversBowled.put(deliveries.getBowler(), 1);
						}
						else {
							int newRuns = bowlerMap2016RunsGiven.get(deliveries.getBowler()) + runs;
							int overs = bowlerMap2016OversBowled.get(deliveries.getBowler())+1;
							
							bowlerMap2016RunsGiven.put(deliveries.getBowler(), newRuns);
							
							bowlerMap2016OversBowled.put(deliveries.getBowler(), overs);
						}
						
					}
				}
			}
			
			if("2017".equals(match.getSeason()) ) {

				for(Deliveries deliveries : iplData.getDeliveriesList()) {
					if( match.getMatchId().equalsIgnoreCase(deliveries.getMatchId()) ) {
						bowlersSet2017.add(deliveries.getBowler());
						int runs = new Integer(deliveries.getTotalRuns() ) - new Integer(deliveries.getByeRuns()) - new Integer(deliveries.getLegByeRuns());
						
						if(null == bowlerMap2017RunsGiven.get(deliveries.getBowler())) {
							bowlerMap2017RunsGiven.put(deliveries.getBowler(), runs);
							bowlerMap2017OversBowled.put(deliveries.getBowler(), 1);
						}
						else {
							int newRuns = bowlerMap2017RunsGiven.get(deliveries.getBowler()) + runs;
							int overs = bowlerMap2017OversBowled.get(deliveries.getBowler())+1;
							
							bowlerMap2017RunsGiven.put(deliveries.getBowler(), newRuns);
							
							bowlerMap2017OversBowled.put(deliveries.getBowler(), overs);
						}
						
					}
				}
			}
		
		}
		
		
		System.out.println("------------------------------------------Result3--------------------------------------");
		
		System.out.println("YEAR"+" | "+"PLAYER"+" | "+"ECONOMY");
		int i = 0;
		for(String bowler : bowlersSet2016) {
			
			if(i==10) {
					break;
			}
			System.out.println("2016"+ " | "+bowler+" | "+(float)bowlerMap2016RunsGiven.get(bowler)/bowlerMap2016OversBowled.get(bowler));
			i++;
		}
		
		i = 0;
		for(String bowler : bowlersSet2017) {
			if(i == 10) {
				break;
			}
			System.out.println("2017"+ " | "+bowler+" | "+ (float) bowlerMap2017RunsGiven.get(bowler)/bowlerMap2017OversBowled.get(bowler));
			i++;
		}

		
	}
}
