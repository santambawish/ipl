package com.fragmadata.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fragmadata.models.Deliveries;
import com.fragmadata.models.IPLDataSet;
import com.fragmadata.models.Matches;
import com.fragmadata.models.NetRateModel;

public class Result4Service {

	Set<String> teamsSet2016 = new HashSet<>();
	Set<String> teamsSet2017 = new HashSet<>();
	
	Set<String> teamsSet2016BattingOvers = new HashSet<>();
	Set<String> teamsSet2017BattingOvers = new HashSet<>();
	
	Set<String> teamsSet2016BowlingOvers = new HashSet<>();
	Set<String> teamsSet2017BowlingOvers = new HashSet<>();
	
	Map<String,Integer> teamMap2016RunsScored = new HashMap<>();
	Map<String,Integer> teamMap2017RunsScored = new HashMap<>();
	
	Map<String,Integer> teamMap2016OversFaced = new HashMap<>();
	Map<String,Integer> teamMap2017OversFaced = new HashMap<>();
	
	Map<String,Integer> teamMap2016RunsGiven = new HashMap<>();
	Map<String,Integer> teamMap2017RunsGiven = new HashMap<>();
	
	Map<String,Integer> teamMap2016OversBowled = new HashMap<>();
	Map<String,Integer> teamMap2017OversBowled = new HashMap<>();
	
	
	Map<String,Integer> teamMap2016NetRate = new HashMap<>();
	Map<String,Integer> teamMap2017NetRate = new HashMap<>();
	
	List<NetRateModel> netRateModelList = new ArrayList<>();
	
	public void findResult4(IPLDataSet iplData) {
		
		for (Matches match: iplData.getMatchesList()) {

			if("2016".equals(match.getSeason()) ) {

				for(Deliveries deliveries : iplData.getDeliveriesList()) {
					if( match.getMatchId().equalsIgnoreCase(deliveries.getMatchId()) ) {
						teamsSet2016.add(deliveries.getBattingTeam());
						if(null == teamMap2016RunsScored.get(deliveries.getBattingTeam())) {
							teamMap2016RunsScored.put(deliveries.getBattingTeam(), new Integer(deliveries.getTotalRuns()));
						}
						else {
							int runs = teamMap2016RunsScored.get(deliveries.getBattingTeam()) + new Integer(deliveries.getTotalRuns());
							teamMap2016RunsScored.put(deliveries.getBattingTeam(), runs);
							
						}
						
						if(null == teamMap2016OversFaced.get(deliveries.getBattingTeam())) {
							teamMap2016OversFaced.put(deliveries.getBattingTeam(), 1);
						}
						else if(null != teamMap2016OversFaced.get(deliveries.getBattingTeam()) ) {
								int balls = new Integer(teamMap2016OversFaced.get(deliveries.getBattingTeam())) + 1;
								teamMap2016OversFaced.put(deliveries.getBattingTeam(), balls);
							
						}
						
						
						if(null == teamMap2016RunsGiven.get(deliveries.getBowlingTeam())) {
							teamMap2016RunsGiven.put(deliveries.getBowlingTeam(), new Integer(deliveries.getTotalRuns()));
						}
						else {
							int runs = teamMap2016RunsGiven.get(deliveries.getBowlingTeam()) + new Integer(deliveries.getTotalRuns());
							teamMap2016RunsGiven.put(deliveries.getBowlingTeam(), runs);
							
						}
						
						
						if(null == teamMap2016OversBowled.get(deliveries.getBowlingTeam()) ) {
							teamMap2016OversBowled.put(deliveries.getBowlingTeam(), 1);
						}
						else if(null != teamMap2016OversBowled.get(deliveries.getBowlingTeam()) ) {
								int balls = new Integer(teamMap2016OversBowled.get(deliveries.getBowlingTeam())) + 1;
								teamMap2016OversBowled.put(deliveries.getBowlingTeam(), balls);
						}
						
						
					}
					
				}
			}
		
		



			if("2017".equals(match.getSeason()) ) {

				for(Deliveries deliveries : iplData.getDeliveriesList()) {
					if( match.getMatchId().equalsIgnoreCase(deliveries.getMatchId()) ) {
						teamsSet2017.add(deliveries.getBattingTeam());
						if(null == teamMap2017RunsScored.get(deliveries.getBattingTeam())) {
							teamMap2017RunsScored.put(deliveries.getBattingTeam(), new Integer(deliveries.getTotalRuns()));
						}
						else {
							int runs = teamMap2017RunsScored.get(deliveries.getBattingTeam()) + new Integer(deliveries.getTotalRuns());
							teamMap2017RunsScored.put(deliveries.getBattingTeam(), runs);
							
						}
						
						if(null == teamMap2017OversFaced.get(deliveries.getBattingTeam())) {
							teamMap2017OversFaced.put(deliveries.getBattingTeam(), 1);
						}
						else if(null != teamMap2017OversFaced.get(deliveries.getBattingTeam()) ) {
								int balls = new Integer(teamMap2017OversFaced.get(deliveries.getBattingTeam())) + 1;
								teamMap2017OversFaced.put(deliveries.getBattingTeam(), balls);
							
						}
						
						
						if(null == teamMap2017RunsGiven.get(deliveries.getBowlingTeam())) {
							teamMap2017RunsGiven.put(deliveries.getBowlingTeam(), new Integer(deliveries.getTotalRuns()));
						}
						else {
							int runs = teamMap2017RunsGiven.get(deliveries.getBowlingTeam()) + new Integer(deliveries.getTotalRuns());
							teamMap2017RunsGiven.put(deliveries.getBowlingTeam(), runs);
							
						}
						
						
						if(null == teamMap2017OversBowled.get(deliveries.getBowlingTeam())) {
							teamMap2017OversBowled.put(deliveries.getBowlingTeam(), 1);
						}
						else if(null != teamMap2017OversBowled.get(deliveries.getBowlingTeam())) {
								int balls = new Integer(teamMap2017OversBowled.get(deliveries.getBowlingTeam())) + 1;
								teamMap2017OversBowled.put(deliveries.getBowlingTeam(), balls);
						}
						
						
					}
					
				}
			}
		
		}
		System.out.println("----------------------------------------Result 4------------------------------------------");
	
		
		for(String team : teamsSet2016) {
			float netRate = ((float)teamMap2016RunsScored.get(team)/
					(teamMap2016OversFaced.get(team)/6))* 
					((float)teamMap2016RunsGiven.get(team)/
					(teamMap2016OversBowled.get(team)/6));
			
			NetRateModel netRatemodel = new NetRateModel();
			netRatemodel.setTeam(team);
			netRatemodel.setNetRate(netRate);
			
			netRateModelList.add(netRatemodel);			
			
			//System.out.println(netRate);
		}
		
		float a = netRateModelList.get(0).getNetRate();
		String teamName =  netRateModelList.get(0).getTeam();
		for(int i =1 ; i < netRateModelList.size(); i++) {
			
			if(netRateModelList.get(i).getNetRate() >a ) {
				a = netRateModelList.get(i).getNetRate();
				teamName = netRateModelList.get(0).getTeam();
			}
			
			
		}
		
		System.out.println("2016 highest Net Run Rate is by : "+teamName);
		
		for(String team : teamsSet2017) {
			float netRate = ((float)teamMap2017RunsScored.get(team)/
					(teamMap2017OversFaced.get(team)/6))* 
					((float)teamMap2017RunsGiven.get(team)/
					(teamMap2017OversBowled.get(team)/6));
			
			NetRateModel netRatemodel = new NetRateModel();
			netRatemodel.setTeam(team);
			netRatemodel.setNetRate(netRate);
			
			netRateModelList.add(netRatemodel);			
			
		}
		
		a = netRateModelList.get(0).getNetRate();
		 teamName =  netRateModelList.get(0).getTeam();
		for(int i =1 ; i < netRateModelList.size(); i++) {
			
			if(netRateModelList.get(i).getNetRate() >a ) {
				a = netRateModelList.get(i).getNetRate();
				teamName = netRateModelList.get(0).getTeam();
			}
			
			
		}
		
		System.out.println("2017 highest Net Run Rate is by : "+teamName);
			
			
		
	}
}
