package com.fragmadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.fragmadata.service.ComputationService;

public class Application {

	public static void main(String[] args) {
		
        BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in)); 
          
        String matches = "";
        String deliveries = "";
        
		try {
			System.out.println("Please enter the path path for matches.csv file: ");
			matches = reader.readLine();
			System.out.println("Please enter the path path for deliveries.csv file: ");
			deliveries = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		} 
  
		System.out.println(matches);
        System.out.println(deliveries);    
        
        matches = "E:\\Study\\interview\\Fragma Data Systems\\iplDataSet\\matches.csv";
        deliveries = "E:\\Study\\interview\\Fragma Data Systems\\iplDataSet\\deliveries.csv";
		
		ComputationService computaionService = new ComputationService();
		computaionService.process(matches,deliveries);	
		
	}	
}
