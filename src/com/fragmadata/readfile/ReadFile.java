package com.fragmadata.readfile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.fragmadata.models.Deliveries;
import com.fragmadata.models.Matches;

public class ReadFile {

	public List<Deliveries> readDeliveries(String deliveriesPath) {
		
		List<Deliveries> deliverieslist = new ArrayList<Deliveries>();
		//List<String> lines = this.readFile("E:\\Study\\interview\\Fragma Data Systems\\iplDataSet\\deliveries.csv");
		
		List<String> lines = this.readFile(deliveriesPath);

	    for (String line : lines) {
	    	
	    	Deliveries deliveries = new Deliveries();
	    	
	        String[] array = line.split(",",-1);
	        //System.out.println(array[0]+" "+array[1]+" "+array[2]+" "+array[3]+" "+array[4]+" "+array[5]+" "+array[6]);
	       
	        deliveries.setMatchId(array[0]);
	        deliveries.setInning(array[1]);
	        deliveries.setBattingTeam(array[2]);
	        deliveries.setBowlingTeam(array[3]);
	        deliveries.setOver(array[4]);
	        deliveries.setBall(array[5]);
	        deliveries.setBatsman(array[6]);
	        deliveries.setBowler(array[7]);
	        deliveries.setWideRuns(array[8]);
	        deliveries.setByeRuns(array[9]);
	        deliveries.setLegByeRuns(array[10]);
	        deliveries.setNoBallRuns(array[11]);
	        deliveries.setPenaltyRuns(array[12]);
	        deliveries.setBatsmanRuns(array[13]);
	        deliveries.setExtraRuns(array[14]);
	        deliveries.setTotalRuns(array[15]);
	        
	        deliverieslist.add(deliveries);
	    }
		
	    return deliverieslist;
	}
	
	
	public List<Matches> readMatches(String matchespath) {
		//List<String> lines = this.readFile("E:\\Study\\interview\\Fragma Data Systems\\iplDataSet\\matches.csv");
		
		List<String> lines = this.readFile(matchespath);
		List<Matches> matcheslist = new ArrayList<>();
		
		for (String line : lines) {
	        String[] array = line.split(",",-1);
	     
	        Matches matches = new Matches();
	        
	        matches.setMatchId(array[0]);
	        matches.setSeason(array[1]);
	        matches.setCity(array[2]);
	        matches.setDate(array[3]);
	        matches.setTeam1(array[4]);
	        matches.setTeam2(array[5]);
	        matches.setTossWinner(array[6]);
	        matches.setTossDecision(array[7]);
	        matches.setResult(array[8]);
	        matches.setWinner(array[9]);
	        
	        matcheslist.add(matches);
	       // System.out.println(array[0]+" "+array[1]+" "+array[2]+" "+array[3]+" "+array[4]+" "+array[5]+" |  "+array[6]+" |  "+array[7]+" "+array[8]+" "+array[9]);   
		}	
	return matcheslist;	
	}
	
	public List<String> readFile(String path) {
		
		//System.out.println("Inside readMatches");
		
		File file = new File(path);

	    List<String> lines = null;
		try {
			lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.out.println("System error !!, Unable to read the file");
		}
		
		return lines;
		
	}
}
